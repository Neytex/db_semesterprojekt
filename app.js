/// 1. Http Server mit 'express' einrichten
const express = require('express');
const app = express();
const path = require('path');
app.use(express.static('public'));
const db = require('./public/js/databaseController')
app.set('view engine', 'pug')

app.get('/', (req, res) => {
  //res.sendFile(path.join(__dirname + '/' + 'index.html'));
  console.log("1. " + db.showData());
  res.render('index', { title: 'Node Server', message: db.showData()})

  if (req.query.name !== undefined) {
    console.log(req.query)
    db.insert(req.query.name, req.query.address);
  }
})

app.get('/remove', (req,res) => {
  if (req.query.name !== undefined) {
    console.log(req.query);
    db.remove(req.query.name)
  }

  // Zurück zur Tabellenansicht
  res.redirect("/");
})

app.get('/create', (req,res) => {
  if (req.query.name !== undefined) {
    console.log(req.query);
    db.createTable(req.query.name)
  }

  // Zurück zur Tabellenansicht
  res.redirect("/");
})

/// 2. Server starten und auf localhost:3000 horchen
app.listen(3000, () => {
  console.log(`Example app listening at http://localhost:3000`)
  db.connect();
})
