/// 1. Datenbank connector
const mysql = require('mysql');
const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "nodedb"
});

// Funktion zum verbinden mit der Datenbank
function connect() {
    /// 2. Verbindung zu DB aufbauen
    con.connect((err) => {
        if (err) throw err;
        console.log("DB connected!");

        // Daten lesen und in der Konsole ausgeben
        con.query("SELECT * FROM customers", (err, result) => {
            if (err) throw err;
            console.log(result);

            // Im Ergebnis einen Datensatz finden (z.b. zum löschen der Zeile)
            // for (let i = 0; i < result.length; i++) {
            //     if (result[i].name === 'Foko Inc') {
            //         console.log(result[i].name + " deleted");
            //         let sql = "DELETE FROM customers WHERE name='Foko Inc'";
            //         con.query(sql, function(err) {
            //             if (err) throw err;
            //             console.log("Row successfully deleted!");
            //         });
            //     }
            // }
        });
    });
}

function insert(name, address) {
    let sql = "INSERT INTO customers (name, address) VALUES ('" + name + "', '"+ address +"')";
    con.query(sql, function (err) {
      if (err) throw err;
      console.log("'" + name + "' added");
    });
}

function remove(name) {
    let sql = "DELETE FROM customers WHERE name='" + name + "'";
    con.query(sql, function (err) {
        if (err) throw err;
        console.log("'" + name + "' removed");
    });
}

function createTable(name) {
    let sql = "CREATE TABLE `" + name + "` (\n" +
        "  `id` int(11) NOT NULL AUTO_INCREMENT,\n" +
        "  `name` varchar(255) DEFAULT NULL,\n" +
        "  `address` varchar(255) DEFAULT NULL,\n" +
        "  PRIMARY KEY (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;"
    con.query(sql, function (err) {
        if (err) throw err;
        console.log("Table '" + name + " created");
    })
}

function getDataFromDB() {
    let data = "default"
    return new Promise(resolve =>  {
        con.query("SELECT * FROM customers", (result) => {

            data = result[0].name
            console.log("2. " + data)
        });
    });
    // console.log("3. " + data)
    //
    // return data;
}

async function showData() {
    const data = await getDataFromDB();
    console.log(data);

    return data;
}

module.exports = { connect, insert, remove, createTable, showData }
